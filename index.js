const express = require("express");
// CREATE AN APPLICATION USING EXPRESS---------------



// require express module and save it in a constant:
const app = express();
// app server will listen to port 3000:
const port = 3000;


// set up for allowing the server to handle data from requests
// allows the app to read json data
app.use(express.json());

// extended:true option, will allow us to accept any data type from forms:
app.use(express.urlencoded({extended:true}));

// GET ROUTE:  "/" will we our URI that will reuurn a specific response
app.get("/", (req,res)=> {
	res.send("Hello World");
})

app.get("/hello", (req, res)=> {
	res.send("Hello from /hello endpoint!");
})



// POST ROUTE :create
app.post("/hello", (req, res)=> {
	res.send (`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

// Mock database
let users=[];
// create a sign up post:
app.post("/signup", (req, res)=>{
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} sucessfully registered`);
	}
	else{
		res.send("Please input BOTH username and password");
	}
})

// PUT request for changing the password
app.put("/change-password", (req, res)=>{
	// creates a variable to store the message to be sent back to the client
	let message;

	// creates a for loop that will loop thru the elements of the "users" array
	for(let i=0; 1 < users.length;  i++){
		if(req.body.username===users[i].username){
			users[i].password=req.body.password;
			message=`User ${req.body.username}'s password has been updated.`;
			break;
		}
		else{
			message= "User does not exist"
		}
	}
	res.send(message);
})

// ACTIVITY------------------------------------------------------------------------------
// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.

app.get("/home", (req, res)=> {
	res.send ("Welcome to the home page")
})


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

app.get("/users", (req, res)=> {
	res.send (users)
})

// Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.


app.delete("/delete-user", (req, res)=>{
	
	let message;
 	let foundUser=[];

	for(let i=0; 1 < users.length;  i++){
		if(req.body.username===users[i].username){
			users.splice(users.indexOf(req.body.username));
			message=`User ${req.body.username} has been deleted.`;
			break;
		}
		else{
			 message="User does not exist"
		}
	}

	res.send(message);
})



// tell the server to listen to the port
app.listen(port, ()=> console.log(`Server is running at port ${port}`))

